import { Image, Text, TouchableOpacity, View, FlatList, Button } from 'react-native'
import React, {Component} from 'react'
import styles from '../Main.Styles'

export default class Inbox extends Component {
  render () {
    return (
      <View style={[styles.scene, { backgroundColor: '#ffffff' }]}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
          paddingVertical: 10
        }}>
          <TouchableOpacity
            style={{
              //alignSelf: 'flex-start',
              alignSelf: 'center'
            }}
            onPress={this.onClose}>
            <Image style={{
              height: 30,
              width: 30
            }}
                   source={require('../../../Images/close_button.png')}
            />
          </TouchableOpacity>

          <View style={{
            flex: 1,
            flexDirection: 'row',
            marginHorizontal: 20
          }}>
            <Image style={{
              width: 40,
              height: 40,
              borderRadius: 20
            }}
                   source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhOxXdpcm3LH1kARaZQjrju072O3Van3Ve_j_yItmYmuCAq55K'}}/>
            <View style={{
              flex: 1,
              marginHorizontal: 10
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}>
                <Text style={{
                  fontWeight: 'bold'
                }}>
                  Tom Matthews
                </Text>
                <TouchableOpacity style={{
                  alignSelf: 'flex-end'
                }}>
                  <Text style={{
                    color: '#8670cf'
                  }}>
                    4.8 ★
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <Text style={{

                }}>
                  A1 Aussie Cool  |  43 Reviews
                </Text>
              </View>
            </View>
          </View>

          <TouchableOpacity
            style={{
              alignSelf: 'flex-end'
            }}
            onPress={this.onClose}>
            <Image style={{
              height: 30,
              width: 30
            }}
                   source={require('../../../Images/3-point.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{
          marginTop: 0,
          width: '100%',
          height: 1,
          backgroundColor: '#989B9C'
        }}/>


        <FlatList
          data={this.props.messages}
          renderItem={this.__renderMessage}
          ListFooterComponentStyle={{
            marginLeft: 70,
            marginTop: 10,
            width: 200
          }}
          ListFooterComponent={this.__renderFooter}/>

      </View>
    )
  }

  __renderMessage = ({item}) => (
    <View style={{
      flexDirection: 'row',
      marginLeft: 20,
      marginTop: 20,
      marginRight: 60
    }}>
      <Image style={{
        width: 40,
        height: 40,
        borderRadius: 20
      }} source={{uri: item.avatar}} />
      <View style={{
        marginHorizontal: 10
      }}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
          <Text style={{
            fontWeight: 'bold'
          }}>
            {item.name}
          </Text>
          <Text style={{
            alignSelf: 'flex-end'
          }}>
            {item.date}
          </Text>
        </View>
        <Text>
          {item.content}
        </Text>
      </View>
    </View>
  )

  __renderFooter = () => (
    <Button
      onPress={() => {}}
      title={'CONTACT SUPPORT'}
      color={'#8670cf'}/>
  )

}
