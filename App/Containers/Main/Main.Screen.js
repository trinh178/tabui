import React, { Component } from 'react'
import { Text, View, Dimensions, StyleSheet, Image, Button, TouchableOpacity, FlatList } from 'react-native'
import { Body, Container, Content, Header, Title } from 'native-base'

import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import styles from './Main.Styles'
import Config from 'react-native-config'

import Inbox from './Inbox/Inbox.Screen'
import JobDetails from './JobDetails/JobDetails.Screen'
import { MainAction } from './Main.Action'

import connect from 'react-redux/es/connect/connect'


class MainScreen extends Component {

    constructor (props) {
      super(props)

      this.state = {
        index: 1,
        routes: [
          { key: 'job_details', title: 'Job Details' },
          { key: 'inbox', title: 'Inbox' },
        ],
      }
    }

    componentWillMount (): void {
      this.props.fetchData(MainAction.fetchData());
    }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        initialLayout={{ width: Dimensions.get('window').width}}
        tabBarPosition={'bottom'}
        renderTabBar={this._renderTabBar}
        onIndexChange={index => this.setState({ index })}
      />
    );
  }

  //
    _renderScene = ({route}) => {
      switch (route.key) {
        case 'job_details':
          return (
            <JobDetails images={this.props.data.images}/>
          )
        case 'inbox':
          return (
            <Inbox messages={this.props.data.messages} />
          )
      }
    }

  _renderTabBar = props => {
    return (
      <View style={{
        alignItems: 'center',
        backgroundColor: '#f7f3f2'
      }}>
        <View style={{ flexDirection: 'row', width: '100%' }}>
          <TabBar
            {...props}
            onTabPress={(tab) => {
              this.setState({ index: tab.index })
            }}
            useNativeDriver
            pressColor={'transparent'}
            pressOpacity={70}
            renderIcon={this._renderIcon}
            renderBadge={this._renderBadge}
            renderLabel={this._renderLabel}
            indicatorStyle={styles.indicator}
            style={styles.tabbar}
            tabStyle={styles.tab}
            labelStyle={styles.label}
            bounces={true}>
          </TabBar>
        </View>
        <View style={styles.bottomBar}/>
      </View>
    )
  }

  _renderIcon = ({ route }) => {

  }

  _renderLabel= ({ route }) => {
    return (
      <Text style={{
        color: '#1e1e1e',
        marginBottom: -10
      }}>{route.title}</Text>
    )
  }

  _renderBadge = ({ route }) => {

  }
}

function mapStateToProps (state) {
  return {
    data: state.main
  }
}

function mapDispatchToProps (dispatch) {
  return {
    fetchData: (request) => dispatch(request)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps)(MainScreen)
