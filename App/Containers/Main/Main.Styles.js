import { StyleSheet, Dimensions } from 'react-native'
const {width, height} = Dimensions.get("window")

export default StyleSheet.create({

  scene: {
    flex: 1,
  },
  indicator: {
    backgroundColor: '#1e1e1e',
    height: 2,
    width: width/2 - 40,
    marginLeft: 20,
  },
  tabbar: {
    elevation: 0,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    backgroundColor: '#f7f3f2',
    flex: 1,
  },
  tab: {
    height: 45
  },
  label: {
    //color: 'transparent',
    //fontWeight: '400'
    color: '#1e1e1e',
    backgroundColor: 'red',
    marginBottom: 0
  },
  tabIconContainer: {
    //width: width / 2,
    //height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },

  bottomBar: {
    backgroundColor: '#000',
    width: width / 2 - 40,
    height: 8,
    marginTop: 5,
    marginBottom: 8,
    borderRadius: 5,
  },


  buttonClose: {
    backgroundColor: 'red',
    height: 20,
    width: 20
  }
})
