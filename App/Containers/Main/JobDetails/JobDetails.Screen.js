import React, {Component} from 'react'
import { FlatList, Image, Text, TouchableOpacity, View } from 'react-native'
import styles from '../Main.Styles'

export default class JobDetails extends Component{
  render () {
    return (
      <View style={[styles.scene, { backgroundColor: '#ffffff' }]}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
          paddingVertical: 10
        }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-start'
            }}
            onPress={() => {}}>
            <Image style={{
              height: 30,
              width: 30
            }}
                   source={require('../../../Images/close_button.png')}
            />
          </TouchableOpacity>

          <Text style={{
            flex: 1,
            textAlignVertical: 'center',
            marginLeft: 30,
            fontWeight: 'bold'
          }}>
            Electrician
          </Text>

          <TouchableOpacity
            style={{
              alignSelf: 'flex-end'
            }}
            onPress={() => {}}>
            <Image style={{
              height: 30,
              width: 30
            }}
                   source={require('../../../Images/3-point.png')}
            />
          </TouchableOpacity>
        </View>


        <View style={{
          backgroundColor: '#3c3465',
          alignSelf: 'baseline',
          width: '100%',
          paddingLeft: 30,
          paddingRight: 30,
          paddingTop: 10,
          paddingBottom: 20
        }}>
          <View style={{
            flexDirection: 'row',
          }}>
            <Image style={{
              height: 20,
              width: 20 }}
                   source={require('../../../Images/clock.png')}/>
            <Text style={{
              color: '#fff',
              marginLeft: 10
            }}>10:30 AM - 12:30 AM | Fri 08 Sep 2018</Text>
          </View>


          <View style={{
            flexDirection: 'row',
            marginTop: 5
          }}>
            <Image style={{
              height: 20,
              width: 20
            }}
                   source={require('../../../Images/place.png')}
            />
            <Text style={{
              color: '#fff',
              marginLeft: 10
            }}>13 Springhill Bottom Rd, TAS, Joondalup</Text>
          </View>


        </View>

        <View style={{
          marginHorizontal: 10
        }}>
          <Text style={{
            color: '#989B9C',
            marginTop: 10
          }}>
            COMMENTS
          </Text>
          {
            this.__renderComment()
          }
        </View>
        <View style={{
          marginTop: 10,
          width: '100%',
          height: 1,
          backgroundColor: '#989B9C'
        }}/>

        <View style={{
          marginHorizontal: 10
        }}>
          <Text style={{
            marginTop: 10,
            fontWeight: 'bold'
          }}>
            Estimate
          </Text>

          <View style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <Text style={{
              alignSelf: 'flex-start'
            }}>
              Minimum charge
            </Text>

            <Text style={{
              alignSelf: 'flex-end',
              fontWeight: 'bold',
              fontSize: 16
            }}>
              $150.00
            </Text>
          </View>

          <View style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <Text style={{
              alignSelf: 'flex-start'
            }}>
              10% GST
            </Text>

            <Text style={{
              alignSelf: 'flex-end',
              fontWeight: 'bold',
              fontSize: 16
            }}>
              $15.00
            </Text>
          </View>
        </View>

        <View style={{
          marginTop: 10,
          width: '100%',
          height: 1,
          backgroundColor: '#989B9C'
        }}/>

        <View style={{
          marginHorizontal: 10
        }}>
          <View style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <Text style={{
              alignSelf: 'flex-start',
              fontWeight: 'bold'
            }}>
              Total
            </Text>

            <Text style={{
              alignSelf: 'flex-end',
              fontWeight: 'bold',
              fontSize: 18
            }}>
              $165.00
            </Text>
          </View>
        </View>

        <View style={{
          marginTop: 10,
          width: '100%',
          height: 1,
          backgroundColor: '#989B9C'
        }}/>

        <Text style={{
          marginHorizontal: 10,
          marginTop: 10
        }}>
          If required any additional parts or labour hours will be discussed onsite before we commence the job.
        </Text>

      </View>
    )
  }

  //
  __renderComment = () => (
    <View>
      <Text style={{
        marginTop: 10
      }}>
        I bought it 2 years ago, I never cleaning it before, so it has been so dusty and dismissed smell.
      </Text>
      <FlatList style={{
        marginTop: 10
      }}
                data={this.props.images}
                renderItem={this.__renderCommentItem}
                horizontal={true}
      />
    </View>
  );
  __renderCommentItem = ({item}) => (
    <Image style={{
      width: 90,
      height: 60,
      borderRadius: 10,
      margin: 5
    }} source={{uri: item.uri}}/>
  )
}
