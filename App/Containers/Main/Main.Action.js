import {createActions} from 'reduxsauce'

const {Types, Creators } = createActions({
  fetchData: []
})

export const MainTypes = Types
export const MainAction = Creators
