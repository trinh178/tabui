import {createReducer} from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { MainTypes } from './Main.Action'

const INITIAL_STATE = Immutable({
  images: [
    {uri: 'https://images.unsplash.com/photo-1475855581690-80accde3ae2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'},
    {uri: 'https://images.unsplash.com/photo-1475855581690-80accde3ae2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'},
    {uri: 'https://images.unsplash.com/photo-1475855581690-80accde3ae2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'},
    {uri: 'https://images.unsplash.com/photo-1475855581690-80accde3ae2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'},
    {uri: 'https://images.unsplash.com/photo-1475855581690-80accde3ae2b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'}
  ],
  messages: [
    {avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8aLU445sdlOSaLZ44PSp4u8TF30BtOA305PzAAuP8L_LrY1BY',
      name: 'Fixle',
      date: '09:05PM',
      content: 'Our labelled farm moans near the moral. A percent basket walks. Near the island ascends a captain. The calm shines with a nose.'},
    {avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8aLU445sdlOSaLZ44PSp4u8TF30BtOA305PzAAuP8L_LrY1BY',
      name: 'Fixle',
      date: '09:05PM',
      content: 'Our labelled farm moans near the moral. A percent basket walks. Near the island ascends a captain. The calm shines with a nose.'}
  ]
})

export const reducer = createReducer(INITIAL_STATE, {
  [MainTypes.FETCH_DATA]: (state, action) => {
    console.log("FETCH_DATA", INITIAL_STATE);
    console.log("FETCH_DATA", action);
    return {
      ...INITIAL_STATE
    }
  }
})
