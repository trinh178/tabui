/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.beesightsoft.tabui;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.beesightsoft.tabui";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 431747;
  public static final String VERSION_NAME = "0.0.2";
  // Fields from default config.
  public static final String API_URL = "http://demo.beesightsoft.com/api/";
  public static final String ENV = "ENV";
  public static final String ROOT_URL = "http://demo.beesightsoft.com/";
}
